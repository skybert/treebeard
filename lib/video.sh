treebeard_install_video() {
  treebeard_os_pkg_install escenic-video
}

treebeard_verify_video() {
  test -d /usr/share/escenic/escenic-video/lib

  if [ -r /etc/debian_version ]; then
    apt-cache show escenic-video |
      grep Description: |
      grep -q 'Content Store'
  fi

  local _url=http://localhost:8080/openapi.json
  curl -s "${_url}"  |
    jq .paths |
    grep -q /webservice/escenic/video || {
    treebeard_roars "${_url} doesn't contain /webservice/escenic/video"
  }
}
