treebeard_install_poll() {
  treebeard_os_pkg_install escenic-poll
}

treebeard_verify_poll() {
  test -d /usr/share/escenic/escenic-poll/lib

  if [ -r /etc/debian_version ]; then
    apt-cache show escenic-poll |
      grep Maintainer: |
      grep -q 'Stibo DX <support@stibodx.com>' ||
      {
        printf "🌳 🔥 Wrong package meta fields: apt-cache show %s\\n" "escenic-poll"
      }
  fi

  local _url=http://localhost:8080/openapi.json
  curl -s "${_url}"  |
    jq .paths |
    grep -q /poll-ws/poll/ || {
    treebeard_roars "${_url} doesn't contain /poll-ws/poll/"
  }

}
