treebeard_install_graphics_converter() {
  if [ -r /etc/debian_version ]; then
    export DEBIAN_FRONTEND=noninteractive
    treebeard_run apt-get install --yes ghostscript imagemagick
  fi

  treebeard_os_pkg_install cue-graphics-converter
}


treebeard_verify_graphics_converter() {
  test -r /usr/share/escenic/cue-graphics-converter/lib/changelog.jar

  if [ -r /etc/debian_version ]; then
    apt-cache show cue-graphics-converter |
      grep Description: |
      grep -q 'CUE Graphics Changelog Daemon'
  fi
}
