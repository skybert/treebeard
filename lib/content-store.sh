
treebeard_install_content_store() {
  treebeard_os_pkg_install escenic-content-engine
}

treebeard_verify_content_store() {
  sleep 1
  treebeard_os_verify_systemd_unit_is_running cue-content-store
  treebeard_os_process_should_run "/etc/escenic/engine/content-engine.yaml"
}

