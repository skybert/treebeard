#! /usr/bin/env bash

treebeard_install_indexer() {
  treebeard_os_pkg_install escenic-indexer
}

treebeard_verify_indexer() {
  # sleep 3
  treebeard_os_verify_systemd_unit_is_running cue-indexer
  treebeard_os_process_should_run "/etc/escenic/indexer/indexer.yaml"
  curl -s  http://localhost:8180/openapi.json | jq  .paths | grep -q /admin/status
}

treebeard_verify_indexer_presentation() {
  treebeard_os_verify_systemd_unit_is_running cue-indexer-presentation
  treebeard_os_process_should_run "/etc/escenic/indexer/indexer-presentation.yaml"
  curl -s  http://localhost:8280/openapi.json | jq  .paths | grep -q /admin/status
}
