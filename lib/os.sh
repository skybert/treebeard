treebeard_os_pkg_install() {
  if [ -r /etc/debian_version ]; then
    export DEBIAN_FRONTEND=noninteractive
    treebeard_run apt-get install --yes "$@"
  else
    printf "🌳 Manually install: %s\\n" "${@}"
  fi
}

treebeard_os_verify_systemd_unit_is_running() {
  systemctl restart "${1}"
  systemctl status "${1}" |
    grep -q 'Active: active (running)' || {
    treebeard_roars "Check: systemctl status ${1}"
  }
}

treebeard_os_pkg_remove_all_stibodx_packages() {
  treebeard_hums "Removing all Stibo DX packages previously installed ..."
  if treebeard_os_is_debian; then
    treebeard_run apt-get remove --purge --yes escenic-* cue-* || true
  fi
}

treebeard_os_pkg_update_cache() {
  if [ -r /etc/debian_version ]; then
    treebeard_run apt-get update
  fi
}

treebeard_os_pkg_configure_manager() {
  if [ -r /etc/debian_version ]; then
    echo deb "${APT_SCHEME-https}://${APT_MACHINE-apt.escenic.com}" "${APT_CODENAME-silicon}" main non-free \
         > /etc/apt/sources.list.d/stibodx.list
    echo machine "${APT_MACHINE-apt.escenic.com}" login "${APT_USER}" password "${APT_PASSWORD}" \
         > /etc/apt/auth.conf.d/stibodx.conf
  fi
}

treebeard_os_process_should_run() {
  ps auxww |
    grep -q "${1}" || {
    treebeard_roars "A process should've run with: ${1}"
  }
}


treebeard_os_info() {
  printf "🌳 Running on: %s\\n" \
         "$(sed -rn 's#PRETTY_NAME=\"(.*)\"#\1#p' /etc/os-release)"
  if treebeard_os_is_debian; then
    printf "🌳 Stibo DX sources: %s\\n" \
           "$(cat /etc/apt/sources.list.d/stibodx.list)"
  fi
}

treebeard_os_is_debian() {
  test -r /etc/debian_version
}
