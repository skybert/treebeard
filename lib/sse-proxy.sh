
treebeard_install_sse_proxy() {
  treebeard_os_pkg_install escenic-sse-proxy
}

treebeard_verify_sse_proxy() {
  source /etc/escenic/sse-proxy/sse-proxy.conf

  test -d "${sse_proxy_lib_dir-/tmp}" || {
    printf "🔥 Failed: %s defines sse_proxy_lib_dir=%s, but it doesn't exist.\\n" \
           /etc/escenic/sse-proxy/sse-proxy.conf \
           "${sse_proxy_lib_dir}"
  }

}
